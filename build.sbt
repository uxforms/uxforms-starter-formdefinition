
import scala.concurrent.duration._

val basePackage = "com.example.form" // TODO: Change this to be the package in which your FormDefinitionFactory and Activator classes live

enablePlugins(SbtOsgi, FormDefinitionPlugin, BuildInfoPlugin)

buildInfoPackage := s"$basePackage.build"

buildInfoKeys := Seq[BuildInfoKey](
  name,
  "artifact" -> (Compile / packageBin / artifactPath).value,
  themeName,
  retentionPeriod
)

buildInfoObject := "MyFormDefinitionBuildInfo"

organization := "com.example" // TODO: Change this to be your organisation

name := "changeme" // TODO: Change this to be the path on which your form will be deployed

themeName := "uxforms" // TODO: Change this to the name of the theme to use in your form

retentionPeriod := 30.minutes // TODO: Change this to the length of time form data should be stored. I.e. the duration of the user's session.

formDefinitionClass := s"$basePackage.MyFormDefinitionFactory" // TODO: Change this to be the fully qualified class name of your FormDefinitionFactory

scalaVersion := "2.11.7"

test := ((Test / test) dependsOn OsgiKeys.bundle).value

libraryDependencies ++= {
  Seq(
    "com.uxforms" %% "test" % "15.26.1" % Test,
    "org.scalatest" %% "scalatest" % "2.2.4" % Test
  )
}

// We must export our form definition factory and activator's package to the OSGi context
// so that UX Forms can load and execute this form definition.
OsgiKeys.exportPackage := Seq(s"$basePackage.*")

// Register our jar as a FormDefinition in the OSGi context
OsgiKeys.bundleActivator := Some(s"$basePackage.Activator")

// Prevent our own form classes from also being imported
OsgiKeys.importPackage := (s"!$basePackage.*" +: OsgiKeys.importPackage.value)

// Don't assume package names are the same as the project name
OsgiKeys.privatePackage := OsgiKeys.privatePackage.value.filterNot(_ == s"${OsgiKeys.bundleSymbolicName.value}.*")

// Add the *names* of jarfiles you want to be embedded within this form's jar.
// Typically used to include third-party libs (and any names of their transitive dependent jars).
// Add your jar name prefixes to embeddedJarNames, and the filter and embeddedJars property below will do the rest for you.
val embeddedJarNames = Seq.empty
val embeddedJarFilter: (File) => Boolean = f => embeddedJarNames.exists(jarName => f.getName.startsWith(jarName))
OsgiKeys.embeddedJars := (Compile / Keys.externalDependencyClasspath).value map(_.data) filter embeddedJarFilter

// Force the build to use Java 8
javacOptions ++= Seq("-source", "1.8", "-target", "1.8")
scalacOptions := Seq("-target:jvm-1.8")
OsgiKeys.requireCapability := "osgi.ee;filter:=\"(&(osgi.ee=JavaSE)(version=1.8))\""
